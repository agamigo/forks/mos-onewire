# OneWire library for Mongoose OS

This library provides support for
[1-wire](https://en.wikipedia.org/wiki/1-Wire) protocol for Mongoose OS.

Refer to the [API documentation](https://mongoose-os.com/docs/api/mgos_onewire.h.html).
